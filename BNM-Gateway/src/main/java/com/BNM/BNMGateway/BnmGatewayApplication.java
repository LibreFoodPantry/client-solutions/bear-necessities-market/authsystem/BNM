package com.BNM.BNMGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BnmGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BnmGatewayApplication.class, args);
	}

}
