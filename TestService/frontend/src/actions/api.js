import { CONSTANTS } from "../actions/index";

export const accessDenied = (url) => ({
  type: CONSTANTS.ACCESS_DENIED,
  payload: {
    url,
  },
});

export const apiError = (error) => ({
  type: CONSTANTS.API_ERROR,
  payload: {
    error,
  },
});
