import axios from "axios";
import { apiError, accessDenied } from "../actions/api";
import { CONSTANTS } from "../actions";

const apiMiddleware = ({ dispatch }) => (next) => (action) => {
  const result = next(action);

  if (action.type !== CONSTANTS.API) return result;

  console.log("Api call");
  const { url, method, data, accessToken, onSuccess, headers } = action.payload;

  // Most HTTP clients use params for GET and DELETE operations while use data for a POST or UPDATE
  const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

  // axios default configs
  axios.defaults.baseURL =
    process.env.REACT_APP_API_PATH || "http://localhost:3100";
  axios.defaults.headers.common["Content-Type"] = "application/json";
  axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

  axios
    .request({
      url,
      method,
      headers,
      [dataOrParams]: data,
    })
    .then(({ data }) => {
      console.log(data);
      if (data) {
        console.log("API call success. ");
        dispatch(onSuccess(data));
      }
    })
    .catch((error) => {
      if (error.response && error.response.status === 403) {
        dispatch(accessDenied(window.location.pathname));
      } else if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        // console.log(error.response.status);
        // console.log(error.response.headers);
        dispatch(apiError(error.response.data));
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("Error", error.message);
      }
    });

  return result;
};

export default apiMiddleware;
