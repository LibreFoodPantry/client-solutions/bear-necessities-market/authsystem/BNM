import React from "react";
import { useKeycloak } from "@react-keycloak/web";
import {
  AppBar,
  Button,
  Toolbar,
  IconButton,
  Typography,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import "./css/header.css";

function Header() {
  const [keycloak] = useKeycloak();

  const handleUserState = () => {
    if (keycloak.authenticated) {
      keycloak.logout();
    } else {
      /**
       * With this example the back arrow will not work. In order to
       * gain this functionality a service will need to access the
       * DOM history object
       */
      keycloak.login();
    }
  };
  return (
    <AppBar position="sticky" className="AppBar">
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="menu"
          className="menuButton"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className="title">
          BNM
        </Typography>
        <Button color="inherit" onClick={handleUserState}>
          {keycloak.authenticated ? "Logout" : "Login"}
        </Button>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
