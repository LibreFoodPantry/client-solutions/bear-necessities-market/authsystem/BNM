import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from "@material-ui/core/Card";
import Icon from "@material-ui/core/Icon";

const useStyles = makeStyles({
    root: {
        width: 300,
        marginTop: 15,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const ApiCard = ({ cardTitle, content, onClick, buttonTitle }) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title}>
                    {cardTitle}
                </Typography>
                <Typography className={classes.pos}>
                    {content}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardActions}>
                <Button
                    size="small"
                    variant="contained"
                    color="primary"
                    endIcon={<Icon>send</Icon>}
                    onClick={onClick}
                >
                    {buttonTitle}
                </Button>
            </CardActions>
        </Card >
    )
}

export default ApiCard;
