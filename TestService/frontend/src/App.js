import React from "react";
import { useKeycloak } from "@react-keycloak/web";
import { useSelector, useDispatch, connect } from "react-redux";
import {
  getProtectedRoute,
  getNonProtectedRoute,
} from "./actions/testApiActions";
import Header from "./components/Header";
import "./components/css/main.css";
import JSONPretty from "react-json-pretty";
import ApiCard from "./components/ApiCard";
var JSONPrettyMon = require("react-json-pretty/dist/monikai");

const App = () => {
  const [keycloak] = useKeycloak();
  const dispatch = useDispatch();

  const unsecuredData = useSelector((state) => state.dataOpen.data);
  const securedData = useSelector((state) => state.dataSecured.data);

  const handleShowKeycloakData = () => {
    if (keycloak.authenticated) {
      return (
        <div className="token-data">
          <p>Keycloak idTokenParsed: </p>
          <JSONPretty
            id="json-pretty"
            theme={JSONPrettyMon}
            data={keycloak.tokenParsed}
            style={{ fontSize: "1.1em" }}
            mainStyle="padding:1em"
            valueStyle="font-size:1.5em"
          />
        </div>
      );
    } else {
      return "";
    }
  };

  return (
    <div>
      <Header />
      <div className="api-cards">
        <ApiCard
          cardTitle="non-protected"
          content={unsecuredData}
          onClick={() => dispatch(getNonProtectedRoute())}
          buttonTitle="Non-Protected"
        />
        <ApiCard
          cardTitle="protected"
          content={securedData}
          onClick={() => dispatch(getProtectedRoute(keycloak.token))}
          buttonTitle="Protected"
        />
        <ApiCard
          cardTitle="Check-Sso"
          content=""
          onClick={() => dispatch()}
          buttonTitle=""
        />
        <ApiCard
          cardTitle="Check-Sso"
          content=""
          onClick={() => dispatch()}
          buttonTitle=""
        />
        <ApiCard
          cardTitle="Admin"
          content=""
          onClick={() => dispatch()}
          buttonTitle=""
        />
        <ApiCard
          cardTitle="Developer"
          content=""
          onClick={() => dispatch()}
          buttonTitle=""
        />
        <ApiCard
          cardTitle="BMN-Admin"
          content=""
          onClick={() => dispatch()}
          buttonTitle=""
        />
      </div>
      {handleShowKeycloakData()}
    </div>
  );
};

export default connect()(App);
