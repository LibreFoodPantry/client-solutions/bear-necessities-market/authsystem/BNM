import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/index";
import apiMiddleware from "../middleware/api";
import thunk from "redux-thunk";

const store = createStore(rootReducer, applyMiddleware(thunk, apiMiddleware));
window.store = store;
export default store;
