import Keycloak from "keycloak-js";
const keycloak = new Keycloak(process.env.PUBLIC_URL + "/keycloak.json");
export default keycloak;
