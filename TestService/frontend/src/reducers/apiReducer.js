import { CONSTANTS } from "../actions/index";

export const apiReducer = (
  state = {
    data: "This is data from an unprotected route... ",
  },
  action
) => {
  switch (action.type) {
    case CONSTANTS.SET_NONPROTECTED_DATA:
      if (action.payload.data) {
        return { data: action.payload.data };
      }
    case CONSTANTS.SET_PROTECTED_DATA:
      return { data: action.payload.data }
    default:
      return state;
  }
};


export const apiReducerProtected = (
  state = {
    data: "This is data from a protected route... ",
  },
  action
) => {
  if (action.type === CONSTANTS.SET_PROTECTED_DATA) {
    return { data: action.payload.data };
  } else {
    return state;
  }
};

export const requestCheckSso = (
  state = {
    data: "This route is a silent check and will not redirect when called, but it will return a 403 forbidden."
  },
  action
) => {
  if (action.type === CONSTANTS.SET_CHECKSSO_DATA) {
    return { data: action.payload.data };
  } else {
    return state;
  }
}

export const requestAdmin = (
  state = {
    data: "This route can only be accessed by a person with role admin on master realm and is set to redirect on call if not authenticated."
  },
  action
) => {
  if (action.type === CONSTANTS.SET_MASTER_ADMIN_DATA) {
    return { data: action.payload.data };
  } else {
    return state;
  }
}

export const requestBnmAdmin = (
  state = {
    data: "This route can only be accessed by a person with the role of admin on BNM realm and is set to redirect on call if not authenticated."
  },
  action
) => {
  if (action.type === CONSTANTS.SET_BNM_ADMIN_DATA) {
    return { data: action.payload.data };
  } else {
    return state;
  }
}

