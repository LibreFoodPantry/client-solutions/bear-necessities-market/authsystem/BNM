import { combineReducers } from "redux";
import { apiReducer } from "./apiReducerProtected";

export default combineReducers({
  dataSecured: apiReducerOpen,
});
