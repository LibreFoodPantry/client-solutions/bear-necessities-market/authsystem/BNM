# Names to identify images and containers of this app
IMAGE_NAME_KEYCLOAK='jboss/keycloak'
CONTAINER_NAME_KEYCLOAK='KeycloakAuthService'

EXECUTE_AS="sudo"

composeDevBuild() {
  docker-compose -f docker-compose-dev.yml up --build

  [ $? != 0 ] && \
    error "Docker-compose-dev Dev build failed !" && exit 100
}

composeDevStart(){
    docker-compose -f docker-compose-dev.yml up
    [ $? != 0 ] && \
    error "Docker-compose-dev up failed !" && exit 100
}

