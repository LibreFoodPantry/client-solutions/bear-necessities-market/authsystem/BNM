require("dotenv").config();
const cors = require("cors");
const express = require("express");
const Keycloak = require("keycloak-connect");
var bodyParser = require("body-parser");
var session = require("express-session");
const app = express();
const port = process.env.PORT || 3100;

app.use(cors());
app.use(bodyParser.json());
// Additional configuration is read from keycloak.json file
// installed from the Keycloak web console.

var server = app.listen(port, () => {
  var host = server.address().address;
  var port = server.address().port;
  console.log(`TestService API listening at http://${host}:${port}`);
});

// Create a session-store to be used by both the express-session
// middleware and the keycloak middleware.

var memoryStore = new session.MemoryStore();

app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true,
    store: memoryStore,
  })
);

var keycloak = new Keycloak({
  store: memoryStore,
});

// Install the Keycloak middleware.
// Specifies that the user-accessible application URL to
// logout should be mounted at /logout
//
// Specifies that Keycloak console callbacks should target the
// root URL.  Various permutations, such as /k_logout will ultimately
// be appended to the admin URL.

app.use(
  keycloak.middleware({
    logout: "/logout",
  })
);

// Keycloak.protect() will enforce that a user must be authenticated before they can access the resource
app.get("/login", keycloak.protect(), (req, res) => {
  res.send({ data: "You are authenticated." });
});

/*
  Keycloak.checkSso() will onlu authenticate if the user is already logged-in.
  If the user has not logged-in, the browser will redirect back to the orginally requested URL and remain unauthenticated
*/
app.get("/check-sso", keycloak.checkSso(), (req, res) =>
  res.send({ data: "Silent Check successful" })
);

app.get("/nonProtectedRoute", (req, res) => {
  res.send({ data: "You successfully called the Non-Protected Route" });
});

app.get("/protectedRoute", keycloak.checkSso(), (req, res) => {
  res.send({ data: "Silent Check successful you are signed in. " });
});

/*
  Demonstrates how to use a specfic role. This role is cross
  realm and can be used for any realm. It is important to note
  that the person must be an admin on the primary realm.
*/
app.get("/admin", keycloak.protect("admin"), (req, res) =>
  res.send({ data: "You are authenticated and an admin" })
);

/*
  to secure resource for specific <realm>:<role>
*/
app.get("/BNM-admin", keycloak.protect("BNM:admin"), (req, res) => {
  res.send({ data: "BNM:admin approved." });
});

/**
 * A normal unprotected base public URL 
 */
app.get("/", (req, res) => res.send({ data: "Hello world" }));